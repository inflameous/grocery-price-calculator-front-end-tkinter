from tkinter import *

root = Tk()
root.title('Grocery Price Calculator')
root.geometry('250x200')
root.resizable(False, False)

v = IntVar(None, 1)

Label(root, text='Choose an item:').grid(row=0, column=1, sticky=W)

Radiobutton(root, text='Banana', variable=v, value=1).grid(row=1, column=1, sticky=W, padx=10)
Radiobutton(root, text='Apples', variable=v, value=2).grid(row=2, column=1, sticky=W, padx=10)
Radiobutton(root, text='Grapes', variable=v, value=3).grid(row=3, column=1, sticky=W, padx=10)
Radiobutton(root, text='Strawberry', variable=v, value=4).grid(row=4, column=1, sticky=W, padx=10)

Label(root, text='Price:').grid(row=5, column=0)
e = Entry(root, state="readonly")
e.insert(END, '0')
e.grid(row=5, column=1)

Button(text='Calculate').grid(row=6, column=0, padx=10)
Entry(root).grid(row=6, column=1)

root.mainloop()